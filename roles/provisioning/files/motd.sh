#!/bin/bash
#
clear

DISTRIB_DESCRIPTION=$(cat /etc/rocky-release)

hostname_upper=$(echo $HOSTNAME | tr a-z A-Z)

echo "Servidor :$hostname_upper"
printf "Contato	: infra@positivo.com.br"
printf "\n"
printf "WebSite	: https://www.positivo.com.br"
printf "\n"
printf "\n"
echo "Welcome to "$(echo $DISTRIB_DESCRIPTION | sed 's/\"//g') "kernel" $(uname -r)
printf "\n"

#logged Users
LoggedInUsers=`who | wc -l`

#system date
date=`date`

#System load
LOAD1=$(cat /proc/loadavg | awk {'print $1'})
LOAD5=$(cat /proc/loadavg | awk {'print $2'})
LOAD15=$(cat /proc/loadavg | awk {'print $3'})

#System uptime
uptime=$(cat /proc/uptime | cut -f1 -d.)
upDays=$((uptime/60/60/24))
upHours=$((uptime/60/60%24))
upMins=$((uptime/60%60))
upSecs=$((uptime%60))

#Roots fs Info
root_free=`df -h / | awk '/\// {print $4}'| grep -v "^$"`
boot_free=`df -h /boot | awk '/\// {print $4}' | grep -v "^$"`

#memory_Usage
memory_all=`free -h | grep Mem | awk '{print $2}'`
memory_usage=`free -h | grep Mem | awk '{print $3}'`
swap_usage=`free -h | grep Mem | awk '{print $4}'`

#Users
users=`users | wc -w`
USER=`whoami`

#Processes
processes=$(ps aux | wc -l)

#interfaces
Interface=$(ip -4 ad | grep 'state UP' | awk -F ":" '!/^[0-9]*: ?lo/ {print $2}')
#Mac=$(ip -4 ad | grep 'global dynamic' | awk '{ print $4 }')
Mac=$(ip -4 link | grep 'ether' | awk '{ print $2 }')
Ip=$(ip -4 ad | grep 'global dynamic' | awk '{ print $2 }')

echo "System infomration as of: $date"
echo
printf "System Load:\t%s %s %s\tSystem Uptime:\t\t%s "days" %s "hours" %s "min" %s "sec"\n" $LOAD1, $LOAD5, $LOAD15 $upDays $upHours $upMins $upSec
printf "Memoriy Total:\t%s\t\t\tMemory Usage:\t%s\tSwap Usage:\t%s\n" $memory_all $memory_all $swap_usage
printf "Free On /:\t%s\t\t\tFree on /boot:\t\t%s\n" $root_free $boot_free
printf "LoggedIN Users:\t%s\t\t\tWhoami:\t\t\t%s\n" $LoggedInUsers $USER
printf "Processes:\t%s\t\t\t\n" $processes
printf "\n"
printf "Interface:\t%s\t\tMAC Address:\t%s\t\tIP Address:\t%s\t\t\n" $Interface $Mac $Ip
printf "\n"
printf "\n"
